'use strict';
'use strict';

const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const helpers = require('./helpers');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CheckerPlugin} = require('awesome-typescript-loader');

const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

const dev = NODE_ENV === 'development';
const prod = NODE_ENV === 'production';
module.exports = {
    context: __dirname + '/src',
    mode: 'development',
    entry: './ts/app.ts',
    output: {
        path: __dirname + '/dist',
        filename: 'app.js'
    },

    watch: dev,
    watchOptions: {
        aggregateTimeout: 100
    },

    devtool: 'cheap-inline-module-source-map',

    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        }),
        prod ? new UglifyJsPlugin() : function () {
        },
        new HtmlWebpackPlugin({
            template: '../src/index.html'
        }),
        new CheckerPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.js$|\.tsx?$/,
                loader: 'babel-loader',
            },
            {
                test: /\.js$/,
                use: ['source-map-loader'],
                enforce: 'pre'
            },
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader'
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.json$/,
                use: ['json-loader']
            }

        ]
    },
    resolve: {
        extensions: ['.js', '.ts', '.css', 'html'],
        alias: {
           'enums/': './src/core/enums/'
        },
        plugins: [
            new TsconfigPathsPlugin({
                configFile: 'tsconfig.json'
            })
        ]
    },
    devServer: {
        host: 'localhost',
        port: '8800',
        contentBase: __dirname + '/dist'
    }
};
