export function formatProperty(target: Object, propertyKey: string) {
    let originProp = target[propertyKey];

    const getter = () =>  {
        return `_${originProp}`;
    };
    const setter = (next) => {
        console.log('updating flavor...');
        originProp = ` ${next} `;
    };
    if (delete target[propertyKey]) {

        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter
        });
    }
}
