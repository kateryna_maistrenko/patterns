export function addedUserName<TFunction extends Function>(target: TFunction) {

    let newConstructor: Function = function(email: string) {
        target[email] = email;
        Object.defineProperty(target, 'userName', {
                value: email.substring(0, email.indexOf('@'))
            });
    };
    return <TFunction>newConstructor;
}
