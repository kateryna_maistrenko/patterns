export function methodLog(target: Object, propertyName: string, propertyDesciptor: PropertyDescriptor): PropertyDescriptor {

    const originMethod = propertyDesciptor.value;

    propertyDesciptor.value = function (...args: any[]) {

        const params = args.map(a => JSON.stringify(a)).join();
        const result = originMethod.apply(this, args);
        const r = JSON.stringify(result);
        console.log(`Call: ${propertyName}(${params}) => ${r}`);
        return result;
    };
    return propertyDesciptor;
}
