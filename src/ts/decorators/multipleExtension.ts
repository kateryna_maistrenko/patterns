export function multipleExtension(...parents: any[]) {

    return function (constructor: Function) {
        parents.forEach(parent => {
            Object.getOwnPropertyNames(parent.prototype).forEach(prop => {
                constructor.prototype[prop] = parent.prototype[prop];
            })
        });
    }
}
