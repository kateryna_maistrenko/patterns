export function readonly (target: Object, propertyKey: string, descriptor: PropertyDescriptor) {
    descriptor.writable = false;
};
