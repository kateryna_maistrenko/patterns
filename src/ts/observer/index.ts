import {EVENTS, PublishingOffice} from "./components/publishingOffice";
import {Client} from "./components/client";

export function createObserver(elem) {
    elem.style.display = 'block';

    const publishingOffice = new PublishingOffice();

    const userJoe = new Client('Joe');
    const userRoss = new Client('Ross');
    const userRachel = new Client('Rachel');
    const userPhoebe = new Client('Phoebe');

    publishingOffice.events.subscribe(EVENTS.PUB_NEWSPAPER, userJoe);
    publishingOffice.events.subscribe(EVENTS.PUB_NEWSPAPER, userRoss);
    publishingOffice.events.subscribe(EVENTS.PUB_NEWSPAPER, userRachel);
    publishingOffice.events.subscribe(EVENTS.PUB_MAGAGINE, userJoe);
    publishingOffice.events.subscribe(EVENTS.PUB_MAGAGINE, userRachel);
    publishingOffice.events.subscribe(EVENTS.PUB_MAGAGINE, userPhoebe);

    publishingOffice.publishMagazine('April 2019');
    publishingOffice.publishNewspaper('08/01/2019');
    publishingOffice.events.unsubscribe(EVENTS.PUB_MAGAGINE, userJoe);
    publishingOffice.events.unsubscribe(EVENTS.PUB_NEWSPAPER, userRachel);
    publishingOffice.publishMagazine('July 2019');
    publishingOffice.publishNewspaper('08/04/2019');
}