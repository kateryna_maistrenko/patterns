import {Subscriber} from "./interfaces/subscriber";

export class Publisher {
    private subscribers: {[key: string]: Subscriber[]} = {};

    subscribe(eventType: string, listener: Subscriber) {
        if (this.subscribers[eventType] === undefined) {
            this.subscribers[eventType] = [];
        }
        if (this.subscribers[eventType].indexOf(listener) === -1) {
            this.subscribers[eventType].push(listener);
        }
    }

    unsubscribe(eventType: string, listener: Subscriber) {
        const deletedIndex = this.subscribers[eventType].indexOf(listener);
        this.subscribers[eventType].splice(deletedIndex, 1);
    }

    publish(eventType: string, data) {
        this.subscribers[eventType].forEach(subscriber => subscriber.update(data));
    }


}