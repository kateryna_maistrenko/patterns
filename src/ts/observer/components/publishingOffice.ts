import {Publisher} from "../publisher";

export enum EVENTS {
    PUB_NEWSPAPER = 'publishNewspaper',
    PUB_MAGAGINE = 'publishMagazine'
}

export class PublishingOffice {
    events: Publisher;

    constructor() {
        this.events = new Publisher();
    }

    publishNewspaper(edition: string): void {
        this.events.publish(EVENTS.PUB_NEWSPAPER, edition);
    }

    publishMagazine(edition: string) {
        this.events.publish(EVENTS.PUB_MAGAGINE, edition);
    }
}