import {Subscriber} from "../interfaces/subscriber";


export class Client implements Subscriber {
    name: string;

    constructor(name: string) {
        this.name = name;
    }

    update(edition: string) {
        console.log(`${this.name} received ${edition}`);
    }
}