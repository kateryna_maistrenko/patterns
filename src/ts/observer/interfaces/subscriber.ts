export interface Subscriber {
    update(data): any
}