export interface IterableBoard {
    board: string[][];
    [Symbol.iterator](): Iterator;
}

export interface Iterable {
    [Symbol.iterator]() : Iterator;
}

interface Iterator {
    next() : IteratorResult;
}

interface IteratorResult {
    value: any;
    done: boolean;
}