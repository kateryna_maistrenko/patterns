import { IterableBoard } from './types';

export const createIterator = (elem: HTMLElement) => {
    elem.style.display = 'block';

    // Generators
    console.log('generator')
    createGenerators();

    // Iterators
    console.log('iterator')
    createIterators();
}

// Generators

function createGenerators () {
    const board = [
        ['a1', 'b1', 'c1'],
        ['a2', 'b2', 'c2'],
        ['a3', 'b3', 'c3']
    ]

    console.log('generator 1');

    const battle = generateNavalBattle(board);
    for (let a of battle) {
        console.log(a);
    }

    console.log('generator 2');

    const battleNew = generateNavalBattleNew(board);
    for (let a of battleNew) {
        console.log(a);
    }
}

function* generateSequence(board, row, start, end) {
    for (let col = start; col <= end; col++) yield board[row][col];
}

function* generateNavalBattle (board) {
    for (let row = 0; row < board.length; row++) {
        yield* generateSequence(board, row, 0, board[row].length-1)
    }
}

function* generateSequenceNew(board, col) {
    for (let row = 0; row < board.length; row++) {
        if (board[row][col]) {
            yield board[row][col]
        }
    }
}

function* generateNavalBattleNew (board) {
    const leng = board[0].length;
    for (let col = 0; col < leng; col++) {
        yield* generateSequenceNew(board, col);
    }
}

// Itertor

function createIterators () {
    console.log('iterator 1');

    const iterableBoard: IterableBoard = createIterableBoard();
    for (let a of iterableBoard) {
        console.log(a);
    }

    console.log('iterator 2');

    const iterableBoardNew: IterableBoard = createIterableBoardNew();
    for (let a of iterableBoardNew) {
        console.log(a);
    }
}

function createIterableBoard (): IterableBoard {
    const iterableBoard = { 
        board: [
            ['a1', 'b1', 'c1'],
            ['a2', 'b2', 'c2'],
            ['a3', 'b3', 'c3']
        ]
    }

    iterableBoard[Symbol.iterator] = function () {
        const board: string[][] = this.board;
        let row = 0;
        let col = 0;
        return {
            next () {
                // if this is the last element in the row, start new row
                if (col === board[row].length) {
                    col = 0;
                    row++;
                }
                const c = col;
                col++;
                // in case you have started a new row, chech if it exists
                if (row < board.length) {
                    return {
                        value: board[row][c],
                        done: false
                    }
                } else {
                    return { done: true}
                }
            }
        }
    }
    return iterableBoard;
}

function createIterableBoardNew (): IterableBoard {
    const iterableBoard = { 
        board: [
            ['a1', 'b1', 'c1'],
            ['a2', 'b2', 'c2'],
            ['a3', 'b3', 'c3']
        ]
    }

    iterableBoard[Symbol.iterator] = function () {
        const board: string[][] = this.board;
        let row = 0;
        let col = 0;
        return {
            next () {
                if (row === board.length) {
                    row = 0; 
                    col++;
                }
                const r = row;
                row++;
                if (col < board[r].length) {
                    return {
                        value: board[r][col],
                        done: false
                    }
                } else {
                    return { done: true }
                }
            }
        }
    }
    return iterableBoard;
}


