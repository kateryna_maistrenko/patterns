import {addedUserName} from '../decorators/addedUserName';
import {sealed} from '../decorators/sealed';
import {readonly} from '../decorators/readonly';
import {methodLog} from '../decorators/methodLog';
import {paramLog} from '../decorators/paramLog';
import {formatProperty} from '../decorators/formatProperty';

@sealed
// @addedUserName
export class User {


    private id: string;
    @formatProperty
    public email: string;

    constructor(id: string, email: string) {
        this.id = id;
        this.email = email;
    }

    @readonly
    public print(message: string): void{
        console.log(message, this.email);
    }

    @methodLog
    public getEmail(): string {
        return this.email;
    }

    public setEmail(newEmail: string) {
        this.email = newEmail;
    }
}
