import Gallery from './components/gallery';


export function createMediator(elem) {
    elem.style.display = 'block';
    let gallery: Gallery;
    gallery = new Gallery(data);
}

const data = [
    {
        "title": "Red cat",
        "category": "Cat",
        "imgUrl": "https://cdn.pixabay.com/photo/2019/03/01/10/03/cat-4027636_960_720.jpg"
    },
    {
        "title": "Black cat",
        "category": "Cat",
        "imgUrl": "https://cdn.shopify.com/s/files/1/0371/9753/articles/black-cat_1400x.progressive.jpg"
    },
    {
        "title": "Gray cat",
        "category": "Cat",
        "imgUrl": "https://www.catster.com/wp-content/uploads/2018/09/A-gray-tabby-cat-with-green-eyes-close-up.jpg"
    },
    {
        "title": "White cat",
        "category": "Cat",
        "imgUrl": "https://i.redd.it/9jva6x5ue9iz.jpg"
    },
    {
        "title": "Brown dog",
        "category": "Dog",
        "imgUrl": "http://imgs.mastphotos.com/wp-content/uploads/2012/10/Cute-Little-Brown-Dog-Sitting.jpg"
    },
    {
        "title": "Golden dog",
        "category": "Dog",
        "imgUrl": "http://www.cutestpaw.com/wp-content/uploads/2014/09/Golden-Dog.jpg"
    },
    {
        "title": "Gray dog",
        "category": "Dog",
        "imgUrl": "https://thenypost.files.wordpress.com/2018/09/siberian-husky.jpg?quality=90&strip=all&w=618&h=410&crop=1"
    },
    {
        "title": "Golden fish",
        "category": "Fish",
        "imgUrl": "http://detsky-mir.com/uploads/images/3/b/0/e/3343/8470918aee.jpg"
    },
    {
        "title": "Betta fish",
        "category": "Fish",
        "imgUrl": "https://brothers-smaller.ru/wp-content/uploads/2015/04/041215_1558_1.jpg"
    },
    {
        "title": "Fussy fish",
        "category": "Fish",
        "imgUrl": "https://3c1703fe8d.site.internapcdn.net/newman/csz/news/800/2019/fussyfishcan.jpg"
    },
    {
        "title": "Disco fish",
        "category": "Fish",
        "imgUrl": "https://www.pecesdeaguadulce.net/wp-content/uploads/2018/05/Pez-disco-e1527759807485.jpg"
    }
];
