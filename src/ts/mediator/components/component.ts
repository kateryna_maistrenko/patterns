import GalleryMediator from '../interfaces/galleryMediator.interface';

export default class Component {

    gallery: GalleryMediator;

    constructor(gallery: GalleryMediator) {
        this.gallery = gallery;
    }

    click(target: EventTarget): void {
        this.gallery.notify(this, 'click', target);
    }
}
