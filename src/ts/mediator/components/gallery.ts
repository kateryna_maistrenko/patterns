import GalleryMediator from '../interfaces/galleryMediator.interface';
import GalleryItem from '../interfaces/galleryItem.interface';
import Picture from "./picture";
import Menu from "./menu";
import Component from "./component";

export default class Gallery implements GalleryMediator {

    galleryItems: GalleryItem[];
    picture: Picture;
    menu: Menu;
    element: HTMLElement;

    constructor(galleryItems: GalleryItem[]) {
        this.element = document.querySelector('.gallery');
        this.galleryItems = galleryItems;
        const defaultPic = this.galleryItems[0].imgUrl;

        this.picture = new Picture(this, defaultPic);
        this.menu = new Menu(this, this.galleryItems);
    }

    notify(sender: Component, eventName: string, target: HTMLElement): void {

        if (sender === this.menu && eventName === 'click') {

            if(target.className === 'category') {

                this.menu.hideSubmenu();
                this.menu.showSubmenu(target.innerText);

            } else if (target.className === 'submenu_item') {

                this.menu.highlightSelectedSubmenu(target.innerText);
                const currentImgUrl = this.menu.getImgUrl(target.innerText);
                this.picture.update(currentImgUrl);
            }
        }  else {
            this.menu.hideSubmenu();
        }

    }
}
