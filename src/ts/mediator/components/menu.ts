import GalleryItem from '../interfaces/galleryItem.interface';
import Component from "./component";
import GalleryMediator from "../interfaces/galleryMediator.interface";
import Submenu from "./submenu";

export default class Menu extends Component {
    menuItems: GalleryItem[];
    isHorizontal: boolean;
    mainMenu: string[];
    elements: NodeListOf<HTMLElement>;

    constructor(gallery: GalleryMediator, menu: GalleryItem[]) {
        super(gallery);
        this.menuItems = menu;
        this.elements = document.querySelectorAll(".menu");

        this.setMenu();
        this.hideSubmenu();
    }

    setMenu(): void {
        this.elements.forEach(menu => {
            menu.innerHTML = this.render(this.menuItems).join('');

            menu.addEventListener('click', function(event: Event) {
                click(event.target);
            });
        });

        const thisMenu = this;
        function click(target: EventTarget) {
            thisMenu.gallery.notify(thisMenu, 'click', target);
        }
    }

    render(menuItems: GalleryItem[]): string[] {
        const categories = this.getCategories(menuItems);
        this.mainMenu = categories;
        return categories.map(category => {
            const submenu = this.createSubmenu(category, menuItems);
            return `<li class="category">${category}<ul>${submenu.join('')}</ul></li>`
        })
    }

    getCategories(items: GalleryItem[]) {
        return items.reduce((categories: string[], item: GalleryItem) => {
            if (categories.indexOf(item.category) === -1) {
                categories.push(item.category);
            }
            return categories;
        }, []);
    }

    createSubmenu(category: string, menuItems: GalleryItem[]): string[] {
        const subcategories = this.getSubcategories(category, menuItems);
        return subcategories.map(menu => menu.render());
    }

    getSubcategories(category: string, items: GalleryItem[]): Submenu[] {
        const subcategories = items.filter(item => item.category === category);
        return subcategories.map(subcategory => new Submenu(subcategory));
    }

    hideSubmenu(): void {
        this.elements.forEach(menu => {
            menu.querySelectorAll('li>ul').forEach(menuItem => menuItem.classList.add('hidden'));
        })
    }

    showSubmenu(category: string) {
        this.elements.forEach(menu => {

            const menuItems = Array.prototype.slice.call(menu.children);
            menuItems.forEach(item => {
                if (item.innerText === category) {
                    item.querySelector('.hidden').classList.remove('hidden');
                }
            })
        })
    }

    highlightSelectedSubmenu(title: string): void {
        this.elements.forEach(menu => {
            const menuItems = Array.prototype.slice.call(menu.children);
            menuItems.forEach(item => {

                const subtitles = Array.prototype.slice.call(item.firstElementChild.children);
                subtitles.forEach(submenu => {
                    submenu.lastChild.style.color = "black";
                    if (submenu.lastChild.innerText === title) {
                        submenu.lastChild.style.color = "white";
                    }
                })

            })
        })
    }

    getImgUrl(title: string): string {
        const menuItem = this.menuItems.find(item => item.title === title);
        return menuItem.imgUrl;
    }
}
