import GalleryItem from "../interfaces/galleryItem.interface";

export default class Submenu {
    menuItem: GalleryItem;

    constructor(menuItem: GalleryItem) {
        this.menuItem = menuItem;

    }

    render(): string {
        const title = this.menuItem.title;
        return `<li class="submenu"><input type="radio" name="animal" value="${title}" id="${title}" />
                <label for="${title}" class="submenu_item">${title}</label></li>`;
    }
}
