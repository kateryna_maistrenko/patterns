import Component from "./component";
import GalleryMediator from "../interfaces/galleryMediator.interface";

export default class Picture extends Component {
    element: HTMLElement;

    constructor(gallery: GalleryMediator, url: string) {
        super(gallery);
        this.element = document.querySelector('.picture');
        this.element.setAttribute('src', url);
    }

    update(url: string) {
        this.element.setAttribute('src', url);
    }
}

