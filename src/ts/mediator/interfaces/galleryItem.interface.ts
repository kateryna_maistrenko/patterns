export default interface GalleryItem {
    title: string,
    category: string,
    imgUrl: string
}
