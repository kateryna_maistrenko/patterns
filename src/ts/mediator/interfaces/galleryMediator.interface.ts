import Component from '../components/component';

export default interface GalleryMediator {
    notify(sender: Component, eventName: string, target: EventTarget): void
}
