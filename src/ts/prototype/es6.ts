import { Prototype } from './interfaces';

export default class Alert implements Prototype {
    message: string;
    private innerCounter: number = 0;
    private static unicCounter: number = 0;

    constructor(message: string) {
        this.innerCounter++;
        Alert.unicCounter++;
        this.message = message;
    }

    static get countUnic(): number {
        return this.unicCounter;
    }
    get countInner(): number {
        return this.innerCounter;
    }

    clone(): this {
        this.innerCounter++;
        const clone = Object.create(this);
        clone.message = this.message;
        return clone;
    }
}