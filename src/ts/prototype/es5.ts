export default function OldAlert(message: string) {
    this.message = message;
    this.innerCounter = 1;
    // private property
    // innerCounter = 1;
    OldAlert.unicCounter++;

    // Object.defineProperty(this, 'countInner', {
    //     get: function() {
    //         console.log(this.message)
    //         return innerCounter;
    //     }
    // });

    OldAlert.prototype.countInner = function() {
        // return innerCounter
        return this.innerCounter;
    }

    OldAlert.prototype.clone = function() {
        // innerCounter++;
        this.innerCounter++;
        var clone = Object.create(this);
        clone.message = this.message;
        return clone;
    }
}

OldAlert.unicCounter = 0;
OldAlert.countUnic = function(): number {
    return this.unicCounter;
}