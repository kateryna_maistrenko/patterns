import Alert from './es6';
import OldAlert from './es5';

export const createPrototype = (elem: HTMLElement) => {
    elem.style.display = 'block';
    // es6
    const alerts: Alert[] = [];
    const warningProto = new Alert('Waring');
    alerts.push(warningProto);
    console.log(warningProto.countInner === 1);
    console.log(Alert.countUnic === alerts.length);
    const compliteProto = new Alert('Complite');
    alerts.push(compliteProto);
    console.log(compliteProto.countInner === 1);
    alerts.push(warningProto.clone());
    console.log(warningProto.countInner === 2);
    alerts.push(compliteProto.clone());
    console.log(compliteProto.countInner === 2);
    console.log(Alert.countUnic + 2 === alerts.length);
    alerts.push(warningProto.clone());
    console.log(warningProto.countInner === 3);
    alerts.push(compliteProto.clone());
    console.log(compliteProto.countInner === 3);
    // es5
    const oldAlerts = [];
    const oldWarningProto = new OldAlert('old warning');
    oldAlerts.push(oldWarningProto);
    console.log(oldWarningProto.countInner() === 1);
    console.log(OldAlert.countUnic() === oldAlerts.length);
    const oldCompliteProto = new OldAlert('old complite');
    oldAlerts.push(oldCompliteProto);
    console.log(oldCompliteProto.countInner() === 1);
    oldAlerts.push(oldWarningProto.clone());
    console.log(oldWarningProto.countInner() === 2);
    oldAlerts.push(oldCompliteProto.clone());
    console.log(oldCompliteProto.countInner() === 2);
    console.log(OldAlert.countUnic() + 2 === oldAlerts.length);
    oldAlerts.push(oldWarningProto.clone());
    console.log(oldWarningProto.countInner() === 3);
    oldAlerts.push(oldCompliteProto.clone());
    console.log(oldCompliteProto.countInner() === 3);
}