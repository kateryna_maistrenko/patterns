import { createMediator } from './mediator/';
import { createObserver } from './observer';
import { createFactory } from './factory_method';
import { createBuilder } from './builder';
import { createAdapter } from './adapter';
import { createAbstractFactory } from './abstract_factory';
import { createSingleton } from './singleton/index';
import { createPrototype } from './prototype/index';
import { createIterator } from './iterator/index';

import '../main.css';

enum Patterns {
    Mediator = 'mediator',
    Observer = 'observer',
    Factory = 'factory',
    Builder = 'builder',
    Adapter = 'adapter',
    AbstractFactory = 'abstractFactory',
    Sigleton = 'singleton',
    Prototype = 'prototype',
    Iterator = 'iterator'
}

(function () {

    const results: NodeListOf<HTMLElement> = document.querySelectorAll('.pattern-result');
    const buttons: NodeListOf<HTMLElement> = document.querySelectorAll('.pattern-list__item');
    const galleryResult: HTMLElement = document.querySelector('.gallery');
    const observerResult: HTMLElement = document.querySelector('.observer');
    const factoryResult: HTMLElement = document.querySelector('.factory');
    const builderResult: HTMLElement = document.querySelector('.builder');
    const adapterResult: HTMLElement = document.querySelector('.adapter');
    const abstractFactoryResult: HTMLElement = document.querySelector('.abstract_factory');
    const singletonResult: HTMLElement = document.querySelector('.singleton');
    const prototypeResult: HTMLElement = document.querySelector('.prototype');
    const iteratorResult: HTMLElement = document.querySelector('.iterator');

    results.forEach(result => result.style.display = 'none');

    buttons.forEach(btn => btn.addEventListener('click', showResult));


    function showResult(e: Event) {
        const pattern = (<HTMLInputElement>e.target).value;
        results.forEach(result => result.style.display = 'none');
        showPattern(pattern);
    }

    const showMediator = createMediator;
    const showObserver = createObserver;
    const showFactoryMethod = createFactory;
    const showBuilder = createBuilder;
    const showAdapterMethod = createAdapter;
    const showAbstractFactory = createAbstractFactory;
    const showSingleton = createSingleton;
    const showPrototype = createPrototype;
    const showIterator = createIterator;
    
    function showPattern(pattern) {
        switch (pattern) {
            case Patterns.Mediator:
                showMediator(galleryResult);
                break;
            case Patterns.Observer:
                showObserver(observerResult);
                break;
            case Patterns.Factory:
                showFactoryMethod(factoryResult);
                break;
            case Patterns.Builder:
                showBuilder(builderResult);
            case Patterns.Adapter:
                showAdapterMethod(adapterResult);
            case Patterns.AbstractFactory:
                showAbstractFactory(abstractFactoryResult);
                break;
            case Patterns.Sigleton:
                showSingleton(singletonResult);
            case Patterns.Prototype:
                showPrototype(prototypeResult);
                break;
            case Patterns.Iterator:
                showIterator(iteratorResult);
                break;    
            default: console.log('no result');
        }
    }



})();
