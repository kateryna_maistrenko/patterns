export interface IBuilder {
    createTitle: () => void;
    createHeader: (name: string) => void;
    createObjectDescr: (...props) => void;
    createConditions: (...props) => void;
    createTerm: (...props) => void;
}

export interface IContract {
    title: HTMLElement;
    header: HTMLElement;
    objectDescr: HTMLElement;
    conditions: HTMLElement;
    term: HTMLElement
}

export enum PeriodRate {
    'Weekly' = 52,
    'Monthly' = 12,
    'Quarterly' = 4
}