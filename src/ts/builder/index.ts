import { CarInsuranceBuilder } from './components/carInsurance/carInsuranceBuilder';
import { CarInsuranceContract } from './components/carInsurance/carInsuranceContract';
import { LifeInsuranceBuilder } from './components/lifeInsurance/lifeInsuranceBuilder';
import { LifeInsuranceContract } from './components/lifeInsurance/lifeInsuranceContract';
import { TourismInsuranceBuilder } from './components/tourismInsurance/tourismInsuranceBuilder';
import { TourismInsuranceContract } from './components/tourismInsurance/tourismInsuranceContract';

export const createBuilder = (elem: HTMLElement) => {
    elem.style.display = 'block';
    const insBtns: NodeListOf<Element> = document.querySelectorAll('.insBtn');
    const formNodes: NodeListOf<Element> = document.querySelectorAll('.insForm');
    const insResult: HTMLElement = document.querySelector('.insResult');
    const forms = Array.from(formNodes);

    // hide all forms
    const hideForms = (): void => {
        forms.forEach(form => (form as HTMLElement).style.display = 'none');
    }
    
    const showForm = (btn: Element) => {
        // reset contract result
        insResult.childNodes.forEach(child => child.remove);

        hideForms();

        if (btn.classList.contains('carInsBtn')) {
            const carForm: Element = forms.find(form => form.classList.contains('carForm'));
            if (carForm) {
                (carForm as HTMLElement).style.display = 'block';
            }
        } else if (btn.classList.contains('lifeInsBtn')) {
            const lifeForm: Element = forms.find(form => form.classList.contains('lifeForm'));
            if (lifeForm) {
                (lifeForm as HTMLElement).style.display = 'block';
            }
        } else {
            const tourismForm: Element = forms.find(form => form.classList.contains('tourismForm'));
            if (tourismForm) {
                (tourismForm as HTMLElement).style.display = 'block';
            }
        }
    }

    // submit forms 
    const submitCarForm = () => {
        const carFormName: HTMLInputElement = document.querySelector('#carFormName');
        const carFormBrand: HTMLInputElement = document.querySelector('#carFormBrand');
        const carFormYear: HTMLInputElement = document.querySelector('#carFormYear');

        const builder = new CarInsuranceBuilder();
        builder.createTitle();
        builder.createHeader(carFormName.value);
        builder.createObjectDescr(carFormBrand.value, carFormYear.value);
        builder.createConditions();
        builder.createTerm();
        const contract: CarInsuranceContract = builder.getContract();
        insResult.appendChild(contract.collectContract());
    }

    const submitLifeForm = () => {
        const lifeFormName: HTMLInputElement = document.querySelector('#lifeFormName');
        const lifeFormInsName: HTMLInputElement = document.querySelector('#lifeFormInsName');
        const lifeFormInsAge: HTMLInputElement = document.querySelector('#lifeFormAge');
        const lifeFormTerm: HTMLInputElement = document.querySelector('#lifeFormTerm');
        const lifeFormPeriod: HTMLSelectElement = document.querySelector('#lifeFormPeriod');

        const builder = new LifeInsuranceBuilder();
        builder.createTitle();
        builder.createHeader(lifeFormName.value);
        builder.createObjectDescr(lifeFormInsName.value, lifeFormPeriod.value);
        builder.createConditions(lifeFormInsAge.value, lifeFormPeriod.value);
        builder.createTerm(lifeFormTerm.value);
        const contract: LifeInsuranceContract = builder.getContract();
        insResult.appendChild(contract.collectContract());
    }

    const submitTourismForm = () => {
        const tourismFormName: HTMLInputElement = document.querySelector('#tourismFormName');
        const tourismFormCountry: HTMLSelectElement = document.querySelector('#tourismFormCountry');
        const tourismFormStart: HTMLInputElement = document.querySelector('#tourismFormStart');
        const tourismFormEnd: HTMLInputElement = document.querySelector('#tourismFormEnd');

        const builder = new TourismInsuranceBuilder();
        builder.createTitle();
        builder.createHeader(tourismFormName.value);
        builder.createObjectDescr(tourismFormCountry.value);
        builder.createConditions(tourismFormStart.value, tourismFormEnd.value);
        builder.createTerm(tourismFormStart.value, tourismFormEnd.value);
        const contract: TourismInsuranceContract = builder.getContract();
        insResult.appendChild(contract.collectContract());
    }

    const submitForm = (e: Event, form: Element) => {
        e.preventDefault();
        if (form.classList.contains('carForm')) {
            submitCarForm();
        } else if (form.classList.contains('lifeForm')) {
            submitLifeForm();
        } else {
            submitTourismForm();
        }
    }

    hideForms();
    insBtns.forEach(btn => btn.addEventListener('click', () => showForm(btn)))
    forms.forEach(form => form.addEventListener('submit', (e) => submitForm(e, form)))
}