import { IContract } from '../../types';

export class TourismInsuranceContract implements IContract {
    title: HTMLElement = null;
    header: HTMLElement = null;
    objectDescr: HTMLElement = null;
    conditions: HTMLElement = null;
    term: HTMLTableElement = null;

    collectContract(): HTMLElement {
        const container: HTMLElement = document.createElement('div');
        container.append(this.title, 
            this.header, 
            this.objectDescr, 
            this.conditions, 
            this.term);
        return container;
    }
}
