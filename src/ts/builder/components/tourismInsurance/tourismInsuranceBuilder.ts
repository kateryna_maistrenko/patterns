import { IBuilder, PeriodRate } from '../../types';
import { TourismInsuranceContract } from './tourismInsuranceContract';
import { createEl, createTable, createTermRange } from '../../helper';

export class TourismInsuranceBuilder implements IBuilder {
    private contract: TourismInsuranceContract;

    constructor() {
        this.reset();
    }
    reset(): void {
        this.contract = new TourismInsuranceContract();
    }
    createTitle(): void {
        this.contract.title = createEl('h2', `Договор страхования для виезда за границу`);
    }
    createHeader(name: string): void {
        this.contract.header = createEl('p', 
        `Данный договор заключен между страховой компанией "Страх" (далее Страховщик) и ${name} (далее Страхователь)`);
    }

    createObjectDescr(country: string): void {
        this.contract.objectDescr = createEl('p', 
        `Страхование жизни для выезда за границу. Страна пребывания - ${country}`)
    }

    createConditions(start: string, end: string): void {
        const amount = 10000;
        const startDate = new Date(start);
        const endDate = new Date(end);
        const diffTime = Math.abs(+endDate - +startDate);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
        const pay = Math.floor(amount / 365 * diffDays);
        this.contract.conditions = createEl('p', 
        `Сумма страхового возмещения: ${amount} UAH. Сумма страхового платежа: ${pay}  UAH`);
    }

    createTerm(start: string, end: string): void {
        this.contract.term = createTable('Период пребывания за границей', 'Начало', 'Конец', start, end);
    }

    getContract(): TourismInsuranceContract {
        const result = this.contract;
        this.contract = new TourismInsuranceContract();
        return result;
    }

    
}