import { IBuilder, PeriodRate } from '../../types';
import { LifeInsuranceContract } from './lifeInsuranceContract';
import { createEl, createTable, createTermRange } from '../../helper';

export class LifeInsuranceBuilder implements IBuilder {
    private contract: LifeInsuranceContract;

    constructor() {
        this.reset();
    }
    reset(): void {
        this.contract = new LifeInsuranceContract();
    }
    createTitle(): void {
        this.contract.title = createEl('h2', `Договор страхования жизни`);
    }
    createHeader(name: string): void {
        this.contract.header = createEl('p', 
        `Данный договор заключен между страховой компанией "Страх" (далее Страховщик) и ${name} (далее Страхователь)`);
    }

    createObjectDescr(name: string, period: string): void {
        this.contract.objectDescr = createTable(``, 
        `Застрахованное лицо`, 
        `Регулярность платежей`, 
        name, period)
    }

    createConditions(age: string, period: string): void {
        const periodRate = this.getTermRate(period);
        const amount = 20000;
        const pay = Math.floor(amount / (100 - +age) / periodRate);
        this.contract.conditions = createEl('p', 
        `Сумма страхового возмещения: ${amount} UAH. Сумма разового страхового платежа: ${pay}  UAH`);
    }

    createTerm(term: string): void {
        const start = new Date();
        const diff: number = start.getFullYear() + parseInt(term);
        const end = new Date(diff, start.getMonth(), start.getDate())
        this.contract.term = createEl('p', 'Срок действия договора: ' + createTermRange(start, end));
    }

    getContract(): LifeInsuranceContract {
        const result = this.contract;
        this.contract = new LifeInsuranceContract();
        return result;
    }

    getTermRate(term: string): PeriodRate {
        switch (term) {
            case 'еженедельно': 
                return PeriodRate.Weekly;
                break;
            case 'ежемесячно': 
                return PeriodRate.Monthly;
            break;
            default: 
                return PeriodRate.Quarterly;
                break;
        }
    }
}