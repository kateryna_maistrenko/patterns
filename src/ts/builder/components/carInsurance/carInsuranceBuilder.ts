import { IBuilder } from '../../types';
import { CarInsuranceContract } from './carInsuranceContract';
import { createEl, createTable } from '../../helper';

export class CarInsuranceBuilder implements IBuilder {
    private contract: CarInsuranceContract;

    constructor() {
        this.reset();
    }
    reset(): void {
        this.contract = new CarInsuranceContract();
    }
    createTitle(): void {
        this.contract.title = createEl('h2', `Договор страхования автомобиля`);
    }
    createHeader(name: string): void {
        this.contract.header = createEl('p', 
        `Данный договор заключен между страховой компанией "Страх" (далее Страховщик) и ${name} (далее Страхователь)`);
    }

    createObjectDescr(object: string, year: string): void {
        this.contract.objectDescr = createTable(`Объект страхования - автомобиль`, `Марка`, `Год выпуска`, object, year);
    }

    createConditions(): void {
        const amount = 10000;
        const pay = 1000;
        this.contract.conditions = createEl('p', 
        `Сумма страхового возмещения: ${amount} UAH. Сумма разового страхового платежа: ${pay}  UAH`);
    }

    createTerm(): void {
        this.contract.term = createEl('p', `Срок действия договора 1 год.`);
    }

    getContract(): CarInsuranceContract {
        const result = this.contract;
        this.contract = new CarInsuranceContract();
        return result;
    }
}