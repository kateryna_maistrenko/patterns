import { IContract } from '../../types';

export class CarInsuranceContract implements IContract {
    title: HTMLHeadElement = null;
    header: HTMLElement = null;
    objectDescr: HTMLTableElement = null;
    conditions: HTMLElement = null;
    term: HTMLElement = null;

    collectContract(): HTMLElement {
        const container: HTMLElement = document.createElement('div');
        container.append(this.title, 
            this.header, 
            this.objectDescr, 
            this.conditions, 
            this.term);
        return container;
    }
}