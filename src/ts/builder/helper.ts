export const createEl = (elem: string, text: string): HTMLElement => {
    const paragraph = document.createElement(elem);
    paragraph.innerText = text;
    return paragraph;
}

export const createTable = (caption, th1: string, th2: string, td1: string, td2: string): HTMLTableElement => {
    const table = document.createElement('table');
    table.setAttribute('border', '1');
    table.style.width = '100%';
    // caption
    const capt = document.createElement('caption');
    capt.innerText = caption;
    // header
    const headerRow = document.createElement('tr');
    const header1 = document.createElement('th');
    const header2 = document.createElement('th');
    header1.innerText = th1;
    header2.innerText = th2;
    headerRow.append(header1, header2);
    // data
    const dataRow = document.createElement('tr');
    const data1 = document.createElement('td');
    const data2 = document.createElement('td');
    data1.innerText = td1;
    data2.innerText = td2;
    dataRow.append(data1, data2);
    // collect table
    table.append(capt, headerRow, dataRow);
    return table;
}

export const createTermRange = (start: Date, end: Date): string => {
        return start.toLocaleDateString() + ' - ' + end.toLocaleDateString()
}