import {multipleExtension} from "../decorators/multipleExtension";

class Animal {

    feed():void {
        console.log("кормим животное");
    }
}

class Transport {

    speed: number=0;
    move(): void {
        if (this.speed == 0) {
            console.log("Стоим на месте");
        }
        else if (this.speed > 0) {
            console.log("Перемещаемся со скоростью " + this.speed + " км/ч");
        }
    }
}

@multipleExtension(Animal, Transport)
export class Horse {
    speed: number=0;
    feed: () => void;
    move: () => void;
}
