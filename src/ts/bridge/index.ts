import TV from './implementation/TV'
import Radio from "./implementation/Radio";
import Remote from "./abstraction/Remote";
import {SmartHouseRemote} from "./abstraction/SmartHouseRemote";

const radio = new Radio()
const tv = new TV()

const radioRemote = new Remote(radio)
const tvRemote = new SmartHouseRemote(tv)

radioRemote.togglePower()
radioRemote.volumeUp()
tvRemote.togglePower()
tvRemote.mute()
