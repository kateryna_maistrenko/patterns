import Remote from "./Remote";

// Abstraction
export class SmartHouseRemote extends Remote {
    public mute () {
        this.device.setVolume(0)
    }
}
