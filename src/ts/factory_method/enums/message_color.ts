export enum MessageColor {
    Success = 'green',
    Danger = 'red'
}