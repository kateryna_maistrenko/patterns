import { Message } from '../interfaces/message.interface';
import { MessageColor } from '../enums/message_color';

export class SuccessMessage implements Message {
    color: MessageColor;
    text: string;

    constructor() {
        this.color = MessageColor.Success;
        this.text = 'Success';
    }
}