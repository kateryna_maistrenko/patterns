import { Message } from '../interfaces/message.interface';
import { MessageColor } from '../enums/message_color';

export class DangerMessage implements Message {
    color: MessageColor;
    text: string;

    constructor() {
        this.color = MessageColor.Danger;
        this.text = 'Danger';
    }
}