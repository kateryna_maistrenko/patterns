import { Message } from './interfaces/message.interface';
import { SuccessMessage } from './components/SuccessMessage';
import { DangerMessage } from './components/DangerMessage';

abstract class Creator {
    abstract createMessage(): Message;

    public setMessage(container: HTMLElement): void {
        const message: Message = this.createMessage();
        container.style.color = message.color;
        container.style.fontWeight = '700';
        container.innerText = message.text;
    }
}

class SuccessCreator extends Creator {
    createMessage(): SuccessMessage {
        return new SuccessMessage();
    }
}

class DangerCreator extends Creator {
    createMessage(): DangerMessage {
        return new DangerMessage();
    }
}

function showSuccessMsg (container: HTMLElement): void {
    new SuccessCreator().setMessage(container);
}

function showDangerMsg (container: HTMLElement): void {
    new DangerCreator().setMessage(container);
}

export function createFactory(elem) {
    elem.style.display = 'block';
    const message: HTMLElement = document.querySelector('.message');
    const successBtn: HTMLElement = document.querySelector('.successBtn');
    const dangerBtn: HTMLElement = document.querySelector('.dangerBtn');

    successBtn.addEventListener('click', () => {showSuccessMsg(message)});
    dangerBtn.addEventListener('click', () => {showDangerMsg(message)});
}

