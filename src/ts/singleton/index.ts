import { ScopedSingleton } from './scopedSingleton';
import { Singleton } from './singletonDecorator';
import { OnlyCat } from './singletonDecorator';

export const createSingleton = (elem: HTMLElement) => {
    elem.style.display = 'block';

    const inst = new ScopedSingleton('vasya');
    const inst2 = new ScopedSingleton('vanya');
    console.log(inst === inst2);
    console.log(inst.name === inst2.name);

    // static instance
    const inst3 = Singleton.getInstance();
    const inst4 = Singleton.getInstance();
    console.log(inst3 === inst4);
    
    // decorator
    console.log('decorator');
    const a = new OnlyCat('cat');
    const b = new OnlyCat('dog');
    console.log(a, b);
    console.log(a === b);
    console.log(a.constructor === b.constructor);
    console.log(a.constructor.name === b.constructor.name);
    console.log(a.__proto__ === b.__proto__);
}
