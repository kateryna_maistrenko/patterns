export function ScopedSingleton(name) {
    var instance;

    ScopedSingleton = function ScopedSingleton() {
        return instance;
    }

    ScopedSingleton.prototype = this;

    instance = new ScopedSingleton(name);

    instance.constructor = ScopedSingleton;
    instance.name = name;
    instance.start = new Date();

    return instance;
}
 