export class Singleton {
    private static instance: Singleton;

    private constructor() {}

    static getInstance(): Singleton {
        if (Singleton.instance) {
            return Singleton.instance;
        } else {
            Singleton.instance = new Singleton();
        }
        return Singleton.instance;
    }
}

function singleton<TFunction extends Function>(constructor: TFunction) {
    Object.defineProperty(constructor, 'instance', { value: null, writable: true });
    const newConstructor: TFunction = function (props: []) {
        if (!constructor['instance']) {
            constructor['instance'] = new constructor(props); 
        }
        return constructor['instance']
    }
    return newConstructor;
}

@singleton
export class OnlyCat {
    name: string;
    start: Date;

    constructor(name: string) {
        this.name = name;
        this.start = new Date();
    }
}
