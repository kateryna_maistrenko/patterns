import { Storage } from './storage';

export class Product {
    storage: Storage;

    constructor(storage: Storage) {
        this.storage = storage;
    }

    addProduct(): void {
        let oldAmount: number = +this.storage.get('eggs');
        this.storage.save('eggs', (++oldAmount).toString())
    }
}