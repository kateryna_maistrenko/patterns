import { Storage } from './storage';
import { CookieStorage } from './cookieStorage';
import { IStorage } from '../types';

export class CookieAdapter implements IStorage {
    cookie: CookieStorage

    constructor(cookie: CookieStorage) {
        this.cookie = cookie;
    }

    save(key: string, value: string): void {
        this.cookie.set(key, value);
    } 

    get(key: string): string {
        return this.cookie.get(key);
    }

    clear(key: string): void {
        this.cookie.set(key, '0');
    }
}