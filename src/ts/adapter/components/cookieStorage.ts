export class CookieStorage {
    set(name: string, value: string): void {
        document.cookie = encodeURIComponent(name) + '=' + encodeURIComponent(value);
    }

    get(name: string): string {
        const cookie = document.cookie.split(';').find(item => {
            return item.startsWith(name)
        });
        return cookie ? cookie.substring(name.length + 1) : '0'
    }
}