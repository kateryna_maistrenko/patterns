import { IStorage } from '../types';

export class Storage implements IStorage {
    save(key: string, value: string):void {
        localStorage.setItem(key, value)
    }

    get(key: string): string {
        return localStorage.getItem(key) ? localStorage.getItem(key) : '0'
    }

    clear(key: string): void {
        localStorage.removeItem(key);
    }
}