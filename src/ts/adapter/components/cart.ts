import { Storage } from './storage';

export class Cart {
    storage: Storage;

    constructor(storage: Storage) {
        this.storage = storage; 
    }

    getProductAmount(): string {
        return this.storage.get('eggs');
    }

    clearCart(): string {
        this.storage.clear('eggs');
        return this.getProductAmount();
    }
}