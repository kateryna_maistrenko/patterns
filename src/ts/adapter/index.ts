import { Storage } from './components/storage';
import { CookieStorage } from './components/cookieStorage';
import { CookieAdapter } from './components/adapter';
import { Product } from './components/product';
import { Cart } from './components/cart';

export function createAdapter(elem: HTMLElement) {
    elem.style.display = 'block';

    const addEggBtn: HTMLElement = document.querySelector('.product_btn');
    const getEggsAmountBtn: HTMLElement = document.querySelector('.cart_btn');
    const result: HTMLElement = document.querySelector('.eggs_result');
    const clearBtn: HTMLElement = document.querySelector('.clear_btn');

    const storage = new CookieAdapter(new CookieStorage());
    const product = new Product(storage);
    const cart = new Cart(storage);
    const addEgg = () => product.addProduct();
    const getEggsAmount = () => {
        result.innerText = cart.getProductAmount();
    }
    const clearEggs = () => cart.clearCart();

    addEggBtn.addEventListener('click', addEgg);
    getEggsAmountBtn.addEventListener('click', getEggsAmount);
    clearBtn.addEventListener('click', clearEggs);
}