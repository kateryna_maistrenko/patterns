export interface IStorage {
    save: (key: string, value: string) => void;
    get: (key: string) => string;
    clear: (key: string) => void;
}