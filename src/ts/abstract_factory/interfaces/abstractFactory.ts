export interface IPlainDeposit {
    rate: number;
    term: number;
    amount: number;
    getResult: () => number;
   }
   
   export interface IComplexDeposit {
    getResult: (plainDeposit: IPlainDeposit) => number;
   }
   
   export interface IAbstractFactory {
    createPlainDeposit: () => IPlainDeposit;
    createComplexDeposit: () => IComplexDeposit;
   }
   
   export interface IDepositAmount {
    plain: number;
    complex: number;
   }