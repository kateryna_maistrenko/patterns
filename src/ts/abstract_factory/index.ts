import { IAbstractFactory, IPlainDeposit,IComplexDeposit, IDepositAmount } from './interfaces/abstractFactory';

class StandartFactory implements IAbstractFactory {
 createPlainDeposit(): IPlainDeposit {
  return new StandartPlainDeposit();
 };
 createComplexDeposit(): IComplexDeposit {
  return new ComplexDeposit();
 };
};

class JuniorFactory implements IAbstractFactory {
 createPlainDeposit(): IPlainDeposit {
  return new JuniorPlainDeposit();
 };
 createComplexDeposit(): IComplexDeposit {
  return new ComplexDeposit();
 };
};

class TaxFreeFactory implements IAbstractFactory {
 createPlainDeposit(): IPlainDeposit {
  return new TaxFreePlainDeposit();
 };
 createComplexDeposit(): IComplexDeposit {
  return new ComplexDeposit();
 };
};


class PlainDeposit implements IPlainDeposit {
 rate: number;
 term: number;
 amount: number;
 constructor(rate: number, term: number, amount: number) {
  this.rate = rate;
  this.term = term;
  this.amount = amount;
 }
 getResult (): number {
  return this.amount * (1 + this.rate / 100 / 12 * this.term);
 }
}

class ComplexDeposit implements IComplexDeposit {
 getResult (plainDeposit: IPlainDeposit): number {
  return Math.round(plainDeposit.amount * Math.pow((1 + plainDeposit.rate / (12 * 100)), plainDeposit.term));
 }
}

class StandartPlainDeposit extends PlainDeposit {
 constructor() {
  super(15, 12, 1000);
 }
}

class JuniorPlainDeposit extends PlainDeposit {
 constructor() {
  super(16, 6, 1000);
 }
}

class TaxFreePlainDeposit extends PlainDeposit {
 constructor() {
  super(13, 12, 1000);
 }
}

function countDepisitAmount(factory: IAbstractFactory): IDepositAmount {
 const plainDeposit = factory.createPlainDeposit();
 const complexDeposit = factory.createComplexDeposit();
 const p = plainDeposit.getResult();
 const c = complexDeposit.getResult(plainDeposit);
 return { 
  plain: plainDeposit.getResult(),
  complex: complexDeposit.getResult(plainDeposit),
 }
}


export function createAbstractFactory(elem) {
 elem.style.display = 'block';

 const standart: HTMLElement = document.querySelector('.standart');
 const junior: HTMLElement = document.querySelector('.junior');
 const taxFree: HTMLElement = document.querySelector('.tax-free');
 const noCap: HTMLElement = document.querySelector('.no_cap');
 const withCap: HTMLElement = document.querySelector('.with_cap');

 function setDepisitAmount(factory: IAbstractFactory): void {
  const { plain, complex } = countDepisitAmount(factory);
  noCap.innerText = '' + plain;
  withCap.innerText = '' + complex;
 }
 standart.addEventListener('click', () => setDepisitAmount(new StandartFactory()));
 junior.addEventListener('click', () => setDepisitAmount(new JuniorFactory()));
 taxFree.addEventListener('click', () => setDepisitAmount(new TaxFreeFactory()));
}